(function() {
  var ACCOUNT_HELLO_MESSAGE = 'Здравствуйте, это звонок от разумного виджета ' +
    'снайперлид, ожидайте соединения';
  var ACCOUNT_SORRY_MESSAGE = 'К сожалению, соединение не может быть ' +
    'установлено';
  var callback = {};
  var calls = {account: null, client: null};
  var accountCallConnected = false;
  var clientCallConnected = false;
  var accountCall = null;
  var clientCall = null;
  var callbackId;
  var accountPhoneNumber;
  var clientPhoneNumber;
  var token;
  var APP_URL = 'http://sniperlead.com/cb';
  var CALL_TIMEOUT = 15 * 1000;
  var callTimeout = {};

  function noop() {}

  function terminate() {
    VoxEngine.terminate();
  }

  function terminateIfAllDisconnected() {
    if (!calls.account && !calls.client) {
      Logger.write('Everybody is disconnected -> terminate session');
      VoxEngine.terminate(); 
    }
  }

  function updateCallbackStatus(cb) {
    var url = APP_URL + '/' + callbackId;
    callback.token = token;
    var method = 'POST';
    var data = JSON.stringify(callback);
    Net.httpRequest(url, function(resp) {
      Logger.write('Request ' + method + ' ' + url + ' ' + data);
      Logger.write('Response status ' + resp.code + ' ' + resp.text);
      (cb || noop)();
    }, {
      method: method,
      postData: data
    });
  }

  function sorryAccount() {
    var call = calls.account;
    call.say(ACCOUNT_SORRY_MESSAGE, Language.RU_RUSSIAN_FEMALE);
    call.addEventListener(CallEvents.PlaybackFinished, function(e) {
      Logger.write('Account playback finished');
      e.call.hangup();
    });
  }

  function hangupIfNotConnected(call) {
    if (call.state() !== 'connected') {
      var customData = call.customData();
      Logger.write('Call (' + customData + ') is not connected after ' +
        (CALL_TIMEOUT / 1000) + 's');
      callTimeout[customData] = true;
      call.hangup();
    }
  }

  VoxEngine.addEventListener(AppEvents.Started, function() {
    var data = VoxEngine.customData();
    var parts = data.split(':');
    if (parts.length < 4) {
      Logger.write('Invalid scenario data: ' + data);
      VoxEngine.terminate();
      return;
    }
    callbackId = parts[0];
    token = parts[1];
    accountPhoneNumber = parts[2];
    clientPhoneNumber = parts[3];
    Logger.write(
      'callbackId: ' + callbackId + ', accountPhoneNumber: ' +
      accountPhoneNumber + ', clientPhoneNumber: ' + clientPhoneNumber);

    Logger.write('Calling to account phoner number ' + accountPhoneNumber);

    var call = calls.account = VoxEngine.callPSTN(accountPhoneNumber);
    call.customData('account_call');
    setTimeout(hangupIfNotConnected.bind(undefined, call), CALL_TIMEOUT);

    call.addEventListener(CallEvents.Connected, function(e) {
      Logger.write('Account connected');
      callback.account_call = callback.account_call || {};
      callback.account_call.successful = true;
      callback.account_call.start_time = +(new Date());
      updateCallbackStatus();
      // e.call.detectVoicemailPrompt();
    });

    call.addEventListener(CallEvents.VoicemailPromptDetected, function(e) {
      Logger.write('Account voicemail prompt detected. Pattern: ' + e.pattern);
      callback.account_call = callback.account_call || {};
      callback.account_call.voicemail = true;
      callback.account_call.voicemail_pattern = e.pattern;
      updateCallbackStatus();

      e.call.hangup();
    });

    call.addEventListener(CallEvents.VoicemailPromptNotDetected, function(e) {
      Logger.write("Account live person detected. Pattern: " + e.pattern);
      callback.account_call = callback.account_call || {};
      callback.account_call.voicemail = false;
      updateCallbackStatus();

      Logger.write('Account playing message');
      e.call.say(ACCOUNT_HELLO_MESSAGE, Language.RU_RUSSIAN_FEMALE);
      e.call.addEventListener(CallEvents.PlaybackFinished, callToClient);
    });

    call.addEventListener(CallEvents.Failed, function(e) {
      Logger.write('Account connection failed');
      callback.account_call = callback.account_call || {};
      callback.account_call.successful = false;
      callback.account_call.timeout = !!callTimeout.account_call;
      callback.account_call.error = {code: e.code, reason: e.reason};
      calls.account = null;
      updateCallbackStatus(terminate);
    });

    call.addEventListener(CallEvents.Disconnected, function(e) {
      Logger.write('Account disconnected');
      callback.account_call = callback.account_call || {};
      callback.account_call.cost = e.cost;
      callback.account_call.duration = e.duration;
      calls.account = null;
      updateCallbackStatus(terminateIfAllDisconnected);
    });
  });

  VoxEngine.addEventListener(AppEvents.Terminating, function() {
    Logger.write('Application terminated');
    callback.terminated = true;
    updateCallbackStatus();
  });

  function callToClient(e) {
    Logger.write('Account playback finished');
    if (e.error) {
      Logger.write('Account playback error: ' + e.error);
    }
    e.call.removeEventListener(CallEvents.PlaybackFinished, callToClient);

    Logger.write('Calling to client');
    var call = calls.client = VoxEngine.callPSTN(clientPhoneNumber);
    call.customData('client_call');
    setTimeout(hangupIfNotConnected.bind(undefined, call), CALL_TIMEOUT);

    call.addEventListener(CallEvents.Connected, function(e) {
      Logger.write('Connected to client');
      callback.client_call = callback.client_call || {};
      callback.client_call.successful = true;
      callback.client_call.start_time = +(new Date());
      updateCallbackStatus();
      e.call.detectVoicemailPrompt();
    });

    call.addEventListener(CallEvents.VoicemailPromptNotDetected, function(e) {
      Logger.write("Client live person detected. Pattern: " + e.pattern);
      callback.client_call = callback.client_call || {};
      callback.client_call.voicemail = false;
      updateCallbackStatus();
      VoxEngine.sendMediaBetween(calls.account, calls.client);
      VoxEngine.easyProcess(calls.account, calls.client);
    });

    call.addEventListener(CallEvents.VoicemailPromptDetected, function(e) {
      Logger.write('Client voicemail prompt detected. Pattern: ' + e.pattern);
      callback.client_call = callback.client_call || {};
      callback.client_call.voicemail = true;
      callback.client_call.voicemail_pattern = e.pattern;
      e.call.hangup();

      updateCallbackStatus(sorryAccount);
    });

    call.addEventListener(CallEvents.Failed, function(e) {
      Logger.write('Client connection failed');
      callback.client_call = callback.client_call || {};
      callback.client_call.successful = false;
      callback.client_call.timeout = !!callTimeout.client_call;
      callback.client_call.error = {code: e.code, reason: e.reason};
      calls.client = null;

      updateCallbackStatus(sorryAccount);
    });

    call.addEventListener(CallEvents.Disconnected, function(e) {
      Logger.write('Client disconnected');
      callback.client_call = callback.client_call || {};
      callback.client_call.cost = e.cost;
      callback.client_call.duration = e.duration;

      updateCallbackStatus(terminateIfAllDisconnected);
    });
  }
}).call(this);